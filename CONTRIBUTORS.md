- Maxime Ancelin <mailto:maxime.ancelin@jailbreak.paris>
- Xavier Arques <mailto:xavierarques@yahoo.com>
- Christophe Benz <mailto:christophe.benz@jailbreak.paris>
- François Bouchet <mailto:francoisbouchet@gmail.com>
- Emmanuel Raviart <mailto:emmanuel@raviart.com>
- Johan Richer <mailto:johan.richer@jailbreak.paris>

Copyright (C) 2018 Xavier Arques, François Bouchet, Paula Forteza & Emmanuel Raviart
Copyright (C) 2019 Christophe Benz & Emmanuel Raviart
